The procedure below has been tested on an Owl with FW version 1.0.1

Prepare an empty USB storage device with a file system that is compatible with the Owl (or use an already existing one that is known to work with the Owl)

https://docs.hak5.org/signal-owl/getting-started/usb-flash-disk-support

Create a payload.sh file in the root of the USB storage device and enter the following, then save and exit the editor.
(There should be no other payload* files on the USB storage device)

--- payload.sh content START

```

#!/bin/bash
/bin/sleep 10
/usr/bin/logger -t Owl "Trying to enter ARMING mode using payload"
/bin/echo "ARMING" > /tmp/MODE

```

--- payload.sh content END

(The sleep and the logger message entry is optional)

Make the payload.sh file executable (it shouldn't really be needed though since the Owl is starting the payload with `bash -c`)

`chmod +x payload.sh`

Attach the prepared USB storage device to the USB port of the Owl that is most far away from the USB cable of the Owl (i.e. the Host Port, NOT the Passthrough Port)

https://docs.hak5.org/signal-owl/getting-started/signal-owl-basics

Connect the Owl to some USB port so that the Owl gets powered up

Wait for a while and the Owl arming mode ESSID should be available to connect to
